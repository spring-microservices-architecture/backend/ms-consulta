package com.mycompany.msconsulta.core.service;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.msconsulta.core.accesodato.entity.Producto;
import com.mycompany.msconsulta.core.interfaces.ProductoRepository;
import com.mycompany.msconsulta.core.interfaces.ProductoServiceInterface;
import com.mycompany.msconsulta.core.util.exception.ResourceNotFoundException;
import com.mycompany.msconsulta.integracion.dto.ProductoDto;
import com.mycompany.msconsulta.integracion.dto.ProductoReadDto;
import com.mycompany.msconsulta.integracion.dto.ResponseDto;

@Service
public class ProductoService implements ProductoServiceInterface{

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	Mapper mapper;

	@Override
	public List<ProductoReadDto> findAll() {
		List<Producto> entityList = productoRepository.findAll();
		List<ProductoReadDto> dtoList = new ArrayList<>();
		ProductoReadDto dto;
		for (Producto obj : entityList) {
			dto = new ProductoReadDto();
			mapper.map(obj, dto);
			dtoList.add(dto);
		}
		return dtoList;
	}

	@Override
	public ProductoReadDto findByCodigo(Long codigo) throws Exception {
		ProductoReadDto dto = new ProductoReadDto();
		Producto entity = productoRepository.findById(codigo)
				.orElseThrow(() -> new ResourceNotFoundException("Código no encontrado: " + codigo));
		mapper.map(entity, dto);
		return dto;
	}

	@Override
	public List<ProductoReadDto> findByDescripcion(String descripcion) throws Exception {
		List<Producto> entityList = productoRepository.findByDescripcion(descripcion);
		List<ProductoReadDto> dtoList = new ArrayList<>();
		ProductoReadDto dto;
		for (Producto obj : entityList) {
			dto = new ProductoReadDto();
			mapper.map(obj, dto);
			dtoList.add(dto);
		}
		return dtoList;
	}

	@Override
	public ResponseDto create(ProductoDto objectDto) throws Exception {
		Producto entity = new Producto();
		mapper.map(objectDto, entity);
		productoRepository.save(entity);
		return new ResponseDto("Producto registrado con éxito.");
	}

	@Override
	public ResponseDto update(Long codigo, ProductoDto objectDto) throws Exception {
		Producto entity = productoRepository.findById(codigo)
				.orElseThrow(() -> new ResourceNotFoundException("Producto no encontrado"));
		mapper.map(objectDto, entity);
		productoRepository.save(entity);
		return new ResponseDto("Producto actualizado con éxito.");
	}

	@Override
	public ResponseDto delete(Long codigo) throws ResourceNotFoundException {
		Producto entity = productoRepository.findById(codigo)
				.orElseThrow(() -> new ResourceNotFoundException("Producto no encontrado"));
		
		try {
			productoRepository.delete(entity);
		} catch (Exception e) {
			throw new RuntimeException("Ocurrió un error al eliminar los datos del producto. " + e.getMessage());
		}
		
		return new ResponseDto("Producto eliminado con éxito.");
	}
}