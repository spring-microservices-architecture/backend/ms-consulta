package com.mycompany.msconsulta.core.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mycompany.msconsulta.core.accesodato.entity.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

	@Query("SELECT p FROM Producto p WHERE p.descripcion LIKE %:descripcion%")
	List<Producto> findByDescripcion(@Param("descripcion") String descripcion);
}