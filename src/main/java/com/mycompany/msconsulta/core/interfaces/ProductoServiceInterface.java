package com.mycompany.msconsulta.core.interfaces;

import java.util.List;

import com.mycompany.msconsulta.integracion.dto.ProductoDto;
import com.mycompany.msconsulta.integracion.dto.ProductoReadDto;
import com.mycompany.msconsulta.integracion.dto.ResponseDto;

public interface ProductoServiceInterface {
	List<ProductoReadDto> findAll();
	ProductoReadDto findByCodigo(Long codigo) throws Exception;
	List<ProductoReadDto> findByDescripcion(String descripcion) throws Exception;
	ResponseDto create(ProductoDto objectDto) throws Exception;
    ResponseDto update(Long codigo, ProductoDto objectDto) throws Exception;
	ResponseDto delete(Long codigo) throws Exception;
}