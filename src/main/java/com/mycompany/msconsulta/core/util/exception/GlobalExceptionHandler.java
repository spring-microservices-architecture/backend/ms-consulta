package com.mycompany.msconsulta.core.util.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.mycompany.msconsulta.integracion.dto.ResponseDto;

@ControllerAdvice
public class GlobalExceptionHandler {

	private final Logger log = LogManager.getLogger(this.getClass());

	@ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
		log.info("Excepción atrapado con ResourceNotFoundException");
		ResponseDto errorMessage = new ResponseDto(ex.getMessage().trim(), request.getDescription(false));
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }

	@ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> serverException(RuntimeException ex, WebRequest request) {
		log.info("Excepción atrapado con RuntimeException");
		ResponseDto errorMessage = new ResponseDto(ex.getMessage().trim(), request.getDescription(false));
		
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
    	log.info("Excepción atrapado con Exception");
    	ResponseDto errorMessage = new ResponseDto(ex.getMessage().trim(), request.getDescription(false));
        return new ResponseEntity<>(errorMessage, HttpStatus.SERVICE_UNAVAILABLE);
    }
}