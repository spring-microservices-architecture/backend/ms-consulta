package com.mycompany.msconsulta.core.accesodato.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
//import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "tbl_producto", schema = "microservicios")
//@SQLDelete(sql = "update tbl_producto set estado = -1 WHERE id = ?", callable = true)
public class Producto implements Serializable {

	private static final long serialVersionUID = -8104762492739097848L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TBL_PRODUCTO")
	@SequenceGenerator(name = "SEQ_TBL_PRODUCTO", sequenceName = "SEQ_TBL_PRODUCTO", schema = "microservicios", allocationSize = 1, initialValue = 1)
	@Column(name = "codigo")
	private Long codigo;

	@Column(name = "descripcion", length = 200, nullable = true)
	private String descripcion;

	private Integer precio;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
}