package com.mycompany.msconsulta.integracion.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.msconsulta.core.interfaces.ProductoServiceInterface;
import com.mycompany.msconsulta.integracion.dto.ProductoDto;
import com.mycompany.msconsulta.integracion.dto.ProductoReadDto;
import com.mycompany.msconsulta.integracion.dto.ResponseDto;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/productos")
@Tag(name = "Productos")
public class ProductoController {

	@Autowired
	private ProductoServiceInterface productoService;

	@Operation(summary = "Lista de productos")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Objeto recuperado exitosamente", content = @Content( schema = @Schema(implementation = ProductoReadDto.class))),
			@ApiResponse(responseCode = "401", description = "No estas autorizado para ver este recurso", content = @Content()),
			@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content()),
			@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content()) })
	@GetMapping
	public ResponseEntity<List<ProductoReadDto>> getAll() {
		return ResponseEntity.ok().body(productoService.findAll());
	}

	@Operation(summary = "Obtener un producto por código")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Objeto recuperado exitosamente", content = @Content( schema = @Schema(implementation = ProductoReadDto.class))),
			@ApiResponse(responseCode = "401", description = "No estas autorizado para ver este recurso", content = @Content()),
			@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content()),
			@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content())})
	@GetMapping("/{codigo}")
	public ResponseEntity<ProductoReadDto> getByCodigo(
			@Parameter(description = "Código de producto del que se recuperará la información", required = true)
			@PathVariable(value = "codigo")
			Long codigo)
			throws Exception {
		return ResponseEntity.ok().body(productoService.findByCodigo(codigo));
	}

	@Operation(summary = "Obtener productos por descripción")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "Objeto recuperado exitosamente", content = @Content( schema = @Schema(implementation = ProductoReadDto.class))),
			@ApiResponse(responseCode = "401", description = "No estas autorizado para ver este recurso", content = @Content()),
			@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso que estaba tratando de alcanzar", content = @Content()),
			@ApiResponse(responseCode = "404", description = "No se encuentra el recurso que intentabas alcanzar", content = @Content()) })
	@GetMapping("/descripcion/{descripcion}")
	public ResponseEntity<List<ProductoReadDto>> getByDescripcion(
			@Parameter(description = "Descripción de producto del que se recuperará la información", required = true)
			@PathVariable(value = "descripcion")
			String descripcion)
			throws Exception {
		return ResponseEntity.ok().body(productoService.findByDescripcion(descripcion));
	}

	@Operation(summary = "Crear producto")
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "201", description = "Registrado con éxito.", content = @Content( schema = @Schema(implementation = ResponseDto.class))),
			@ApiResponse(responseCode = "401", description = "No estas autorizado para acceder a este recurso", content = @Content()),
			@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Recurso no encontrado", content = @Content())
	})
	@PostMapping()
	public ResponseEntity<ResponseDto> create(@Parameter(description = "Objeto a almacenar en la base de datos", required = true) @Valid @RequestBody ProductoDto dataDto) throws Exception {
		return new ResponseEntity<>(productoService.create(dataDto), HttpStatus.CREATED);
	}

	@Operation(summary = "Actualizar producto")
	@ApiResponses(value = { 
			@ApiResponse(responseCode = "200", description = "Actualizado con éxito.", content = @Content( schema = @Schema(implementation = ResponseDto.class))),
			@ApiResponse(responseCode = "401", description = "No estas autorizado para acceder a este recurso", content = @Content()),
			@ApiResponse(responseCode = "403", description = "Está prohibido acceder al recurso", content = @Content()),
			@ApiResponse(responseCode = "404", description = "Recurso no encontrado", content = @Content())
	})
	@PutMapping("/{codigo}")
	public ResponseEntity<ResponseDto> update(
			@Parameter(description = "Id del objeto a actualizar", required = true) @PathVariable(value = "codigo") Long codigo,
			@Parameter(description = "Objeto a actualizar", required = true) @Valid @RequestBody ProductoDto dataDto) throws Exception {

		return new ResponseEntity<>(productoService.update(codigo, dataDto), HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<ResponseDto> delete(
			@Parameter(description = "Id de objeto que se eliminará de la base de datos", required = true)
			@PathVariable(value = "codigo")
			Long codigo) throws Exception{
		
		return new ResponseEntity<>(productoService.delete(codigo), HttpStatus.OK);
	}
	
}