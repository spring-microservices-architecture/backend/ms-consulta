package com.mycompany.msconsulta.integracion.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ProductoReadDto {

	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Long codigo;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private String descripcion;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer precio;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
}